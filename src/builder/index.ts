/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { PhoneNumberCondition } from "./condition";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DEFINED from "../../assets/icon-defined.svg";
import ICON_UNDEFINED from "../../assets/icon-undefined.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:phone-number", "Phone number");
    },
})
export class PhoneNumber extends NodeBlock {
    phoneNumberSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.phoneNumberSlot = this.slots.static({
            type: Slots.String,
            reference: "phone-number",
            label: PhoneNumber.label,
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.phoneNumberSlot);
        this.editor.visibility();
        this.editor.alias(this.phoneNumberSlot);
        this.editor.exportable(this.phoneNumberSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "defined",
                    label: pgettext(
                        "block:phone-number",
                        "Number is not empty"
                    ),
                    icon: ICON_DEFINED,
                },
                {
                    mode: "undefined",
                    label: pgettext("block:phone-number", "Number is empty"),
                    icon: ICON_UNDEFINED,
                },
            ],
            (condition: { mode: TMode; label: string; icon: string }) => {
                this.conditions.template({
                    condition: PhoneNumberCondition,
                    label: condition.label,
                    icon: condition.icon,
                    burst: true,
                    props: {
                        slot: this.phoneNumberSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
