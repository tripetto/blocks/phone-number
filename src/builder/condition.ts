/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DEFINED from "../../assets/icon-defined.svg";
import ICON_UNDEFINED from "../../assets/icon-undefined.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:phone-number", "Verify phone number");
    },
})
export class PhoneNumberCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TMode = "defined";

    get icon() {
        return this.mode === "defined" ? ICON_DEFINED : ICON_UNDEFINED;
    }

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:phone-number",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:phone-number",
                        "empty"
                    )}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:phone-number", "When phone number:"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext(
                                "block:phone-number",
                                "Is not empty"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:phone-number", "Is empty"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "defined")
                ),
            ],
        });
    }
}
